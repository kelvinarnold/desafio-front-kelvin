### DESAFIO ###

Para o desafio utilicei
- ReacJS
- Redux
- Redux thunk
- React router
- Axios
- Sass
- Bootstrap

### Estrutura das pastas do app
```
desafio-front-kelvin/
  layout/
  node_modules/
  public/
  src/
    assets/
      fonts/
      images/
      sass/
    components/
    constant/
    helpers/
    pages/
    redux/
    routes/
    service/
```

A pasta [pages] conten todos os views do app
A pasta [routes] conten todos os enderecos do app
A pasta [helpers] conten funcoes de ajuda comuns do app
A pasta [components] conten todos os componentes do app

### `yarn`
Para instalar todos os pacotes

### `yarn start`
Para iniciar o projeto
[http://localhost:3000](http://localhost:3000)

### `yarn build`
Para buildar o projeto e mimificar os arquivos