import React from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import { PageHome } from './../pages/Home';
import { PageDetail } from './../pages/Detail';

const AppRoutes = () => {
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={PageHome} />
          <Route path="/heroe/:id" exact component={PageDetail} />
          <Redirect to="/"/>
        </Switch>
      </BrowserRouter>
    </>
  )
}

export default AppRoutes;
