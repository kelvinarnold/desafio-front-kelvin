import React, { Component } from 'react';
import CardHeroe from './../../components/CardHeroe';
import AppFilter from './../../components/AppFilter'
class PageHome extends Component {
  constructor() {
    super();
    this.state = {
      currentNivel: '',
      currentFilter: '',
      modalFilter: false
    }
  }
  componentWillMount() {
    this.getAllHeroes();
  }
  getAllHeroes = () => {
    const { actions } = this.props;
    this.setState({
      currentFilter: ''
    })
    actions.getHeroes();
  }
  goHeroeProfile = heroe => {
    this.props.history.push(`/heroe/${heroe.id}`);
  }
  setNivel1 = val => {
    const { actions, heroes } = this.props;
    if (heroes.loadingFilter) {
      return;
    }
    this.setState({currentNivel: val});
    switch (val) {
      case 'Comics':
        actions.getComics();
        break;
      case 'Events':
        actions.getEvents();
        break;
      default:
        actions.getSeries();
        break;
    }
  }
  setNivel2 = val => {
    const { actions, heroes } = this.props;
    if (heroes.loadingFilter) {
      return;
    }
    this.setState({currentFilter: val, modalFilter: false});
    switch (this.state.currentNivel) {
      case 'Comics':
        actions.getHeroeByComic(val.id);
        break;
      case 'Events':
        actions.getHeroesByEvent(val.id);
        break;
      default:
        actions.getHeroesBySerie(val.id);
        break;
    }
  }
  render() {
    const {heroes} = this.props;
    return (
      <div className="app-pages--home d-flex flex-column">
        <div className="row">
          <div className="col-12 mt-4">
            <button className="btn btn-primary title" onClick={() => this.setState({modalFilter: true})}>Show filters</button>
            <button className="btn link title" onClick={() => this.getAllHeroes()}>Show all heroes</button>
          </div>
          <div className="col-12 mt-2">
            <h3 className="link title" onClick={() => this.setState({modalFilter: true})}>{this.state.currentFilter !== '' ? this.state.currentFilter.title : 'All Heroes'}</h3>
          </div>
        </div>
        {
          this.state.modalFilter ?
          <AppFilter
            callbackivel1={val => this.setNivel1(val)}
            callbackivel2={val => this.setNivel2(val)}
            secondFilterList={heroes.filter}
            loading={heroes.loadingFilter}
            error={heroes.error}
            currentNivel={this.state.currentNivel}
            currentFilter={this.state.currentFilter}
            closeBack={() => this.setState({modalFilter: false})}/> :
          null
        }
        {
          heroes.loadingHeroes ?
          <div className="title pt-4 pb-4"><h4>Loading</h4></div> :
          heroes.error ?
          <div className="title pt-4 pb-4"><h4>Something Wrong</h4></div> :
          !heroes.heroesList.length ?
          <div className="title pt-4 pb-4">No heroes available</div> :
          <div className="row">
            {
              heroes.heroesList.map(heroe => {
                return (
                  <div className="col-12 col-sm-6 col-md-4 col-lg-3 mb-2 mt-2" key={heroe.id}>
                    <CardHeroe callback={() => this.goHeroeProfile(heroe)} heroe={{img: `${heroe.thumbnail.path}.${heroe.thumbnail.extension}`, name: heroe.name}}/>
                  </div>
                )
              })
            }
          </div>
        }
      </div>
    )
  }
}

export default PageHome;