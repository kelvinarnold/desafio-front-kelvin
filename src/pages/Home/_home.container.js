import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actions } from './../../redux/Heroes'

import PageHome from './_home';

const mapStateToProps = ({heroes}) => ({
  heroes
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({...actions}, dispatch)
})

const HomeContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(PageHome);

export default HomeContainer;
