import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actions } from './../../redux/Heroes'

import PageDetail from './_detail';

const mapStateToProps = ({heroes}) => ({
  heroes
})

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators({...actions}, dispatch)
})

const DetailContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(PageDetail);

export default DetailContainer;
