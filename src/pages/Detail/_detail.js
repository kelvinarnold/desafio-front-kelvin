import React, { Component } from 'react';
import Navigation from './../../components/Navigation';
import HeroeCover from './../../components/HeroeCover';
import HeroeAvatar from './../../components/HeroeAvatar';
import HeroeBook from './../../components/HeroeBooks';

class PageDetail extends Component {
  componentWillMount() {
    const { actions, match } = this.props;
    actions.getHeroeByID(match.params.id);
  }
  render() {
    const {heroes} = this.props;
    return (
      <div className="app-pages--detail d-flex flex-column">
        {
          heroes.loadingHeroe ?
          <div className="title p-4"><h4>Loading</h4></div> :
          heroes.error ?
          <div className="title p-4"><h4>Something Wrong</h4></div> :
          <>
            <Navigation/>
            <div className="row">
              <HeroeCover cover={`${heroes.heroe.thumbnail.path}.${heroes.heroe.thumbnail.extension}`}/>
            </div>
            <div className="detail-content row">
              <div className="col">
                <div className="heroe-information d-flex">
                  <HeroeAvatar avatar={`${heroes.heroe.thumbnail.path}.${heroes.heroe.thumbnail.extension}`}/>
                  <div className="ml-4 title">
                    <h2>{heroes.heroe.name}</h2>
                    <h2>ID: {heroes.heroe.id}</h2>
                  </div>
                </div>
                <div className="heroe-extra">
                  <HeroeBook title={`Comics`} books={heroes.heroe.comics ? heroes.heroe.comics.items : []}/>
                  <HeroeBook title={`Stories`} books={heroes.heroe.stories ? heroes.heroe.stories.items : []}/>
                  <HeroeBook title={`Events`} books={heroes.heroe.events ? heroes.heroe.events.items : []}/>
                  <HeroeBook title={`Series`} books={heroes.heroe.series ? heroes.heroe.series.items : []}/>
                </div>
              </div>
              {
                heroes.heroe.description ?
                <div className="col-12 col-sm-4">
                  <div className="heroe-description">
                    {heroes.heroe.description}
                  </div>
                </div> : null
              }
            </div>
          </>
        }
      </div>
    )
  }
}

export default  PageDetail;