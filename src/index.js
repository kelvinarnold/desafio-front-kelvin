import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import configStore from './redux/store-config';

import App from './App';

ReactDOM.render(
  <Provider store={configStore}>
    <App/>
  </Provider>, document.getElementById('root'));
