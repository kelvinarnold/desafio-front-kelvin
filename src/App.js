import React from 'react';
import AppRoutes from './routes';


// Components
import AppHeader from './components/AppHeader';

import './assets/sass/app.scss';

function App() {
  return (
    <div className="app">
      <AppHeader/>
      <div className="app-pages container">
        <AppRoutes/>
      </div>
    </div>
  );
}

export default App;
