import HTTP from './../http';

const PUBLIC_API_KEY = 'fdf5565a269a7cec6a32fc3ca9f569ba';

const getHeroes = () => {
  return new Promise((resolve, reject) => {
    HTTP.get(`/v1/public/characters?apikey=${PUBLIC_API_KEY}`)
    .then(res => {
      resolve(res.data.data);
    })
    .catch(err => reject(err))
  })
}

const getHeroesByID = id => {
  return new Promise((resolve, reject) => {
    HTTP.get(`/v1/public/characters/${id}?apikey=${PUBLIC_API_KEY}`)
    .then(res => {
      resolve(res.data.data);
    })
    .catch(err => reject(err))
  })
}

const getComics = id => {
  return new Promise((resolve, reject) => {
    HTTP.get(`/v1/public/comics?apikey=${PUBLIC_API_KEY}`)
    .then(res => {
      resolve(res.data.data);
    })
    .catch(err => reject(err))
  })
}

const getHeroesByComic = id => {
  return new Promise((resolve, reject) => {
    HTTP.get(`/v1/public/comics/${id}/characters?apikey=${PUBLIC_API_KEY}`)
    .then(res => {
      resolve(res.data.data);
    })
    .catch(err => reject(err))
  })
}

const getSeries = () => {
  return new Promise((resolve, reject) => {
    HTTP.get(`/v1/public/series?apikey=${PUBLIC_API_KEY}`)
    .then(res => {
      resolve(res.data.data);
    })
    .catch(err => reject(err))
  })
}

const getHeroesBySerie = id => {
  return new Promise((resolve, reject) => {
    HTTP.get(`/v1/public/series/${id}/characters?apikey=${PUBLIC_API_KEY}`)
    .then(res => {
      resolve(res.data.data);
    })
    .catch(err => reject(err))
  })
}

const getEvents = () => {
  return new Promise((resolve, reject) => {
    HTTP.get(`/v1/public/events?apikey=${PUBLIC_API_KEY}`)
    .then(res => {
      resolve(res.data.data);
    })
    .catch(err => reject(err))
  })
}

const getHeroesByEvent = id => {
  return new Promise((resolve, reject) => {
    HTTP.get(`/v1/public/events/${id}/characters?apikey=${PUBLIC_API_KEY}`)
    .then(res => {
      resolve(res.data.data);
    })
    .catch(err => reject(err))
  })
}

export default {
  getHeroes,
  getHeroesByID,
  getComics,
  getHeroesByComic,
  getSeries,
  getHeroesBySerie,
  getEvents,
  getHeroesByEvent
}