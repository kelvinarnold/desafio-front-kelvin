const API_CONFIG = process.env.NODE_ENV === 'development' ?
  process.env.REACT_APP_API_DEVELOPMENT :
  process.env.REACT_APP_API_PRODUCTION

export default API_CONFIG;