import React from 'react'

const HeroeCover = ({cover}) => {
  return (
    <div className="app-component--heroe__cover" style={{
      backgroundImage: `url(${cover})`
    }}/>
  )
}

export default HeroeCover;