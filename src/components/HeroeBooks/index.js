import React from 'react';

const HeroeBooks = ({title, books}) => {
  if (!books.length) {
    return null;
  }
  return (
    <div className="app-component--heroe__book d-md-flex ">
      <div className="title col-md-auto col-12">{title}:</div>
      <div className="list col">
        <ul>
          {
            books.map((item, key) => <li key={key}>{item.name}</li>)
          }
        </ul>
      </div>
    </div>
  )
}

export default HeroeBooks;