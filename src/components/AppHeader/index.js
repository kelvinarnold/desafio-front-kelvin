import React from 'react';
import { IMG_LOGO, IMG_SITE_BLINDADO } from './../../constants';

const AppHeader = () => {
  return (
    <div className="app-header d-flex align-items-center p-4 position-relative">
      <img src={IMG_SITE_BLINDADO} alt="site-blindado" className="app-header--site" height={100}/>
      <img src={IMG_LOGO} alt="Marvel Logo" className="app-header--logo" height={100}/>
    </div>
  )
}

export default AppHeader;