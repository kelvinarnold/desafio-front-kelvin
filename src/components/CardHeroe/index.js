import React from 'react'

const CardHeroe = ({callback, heroe}) => {
  return (
    <div className="card app-component--card" onClick={() => callback()}>
      <div className="card-img-top card-image"
        style={{
          backgroundImage: `url(${heroe.img})`
        }}>
      </div>
      <div className="card-body">
        <h5 className="card-title">{heroe.name}</h5>
      </div>
    </div>
  )
}

export default CardHeroe;