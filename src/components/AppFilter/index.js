import React from 'react';
import {FILTER_NIVEL_1} from './../../constants';

const AppFilter = ({
  callbackivel1=()=>{}, 
  callbackivel2=()=>{},
  secondFilterList=[],
  loading=false,
  error=false,
  currentNivel=null,
  currentFilter=null,
  closeBack=()=>{}}) => {

  return (
    <div className="app-components--filter">
      <div className="modal">
        <span className="overlay" onClick={() => closeBack()}></span>
        <div className="modal-dialog modal-lg" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <span></span>
              <span aria-hidden="true" onClick={() => closeBack()}>&times;</span>
            </div>
            <div className="modal-body row">
              <div className="col-12 col-md-4">
                <ul className="list title">
                  {FILTER_NIVEL_1.map(item => <li className={`link${currentNivel === item.name ? ` text-primary`:``}`} key={item.id} onClick={() => callbackivel1(item.name)}><h4>{item.name}</h4></li>)}
                </ul>
              </div>
              <div className="col-12 col-md-8">
                <ul className="list title">
                  {
                    loading ?
                    <li>Loading</li> :
                    error ?
                    <li>Something Wrong</li> :
                    !secondFilterList.length ?
                    <li>Select one option</li> :
                    secondFilterList.map(item => <li className={`link${currentFilter.id === item.id ?' text-primary':''}`} key={item.id} onClick={() => callbackivel2(item)}>{item.title}</li>) 
                  }
                </ul>
              </div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary title">REMOVE FILTERS</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AppFilter;