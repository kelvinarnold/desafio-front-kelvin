import React from 'react'

const HeroeAvatar = ({avatar}) => {
  return (
    <div className="app-component--heroe__avatar" style={{
      backgroundImage: `url(${avatar})`
    }}/>
  )
}

export default HeroeAvatar;