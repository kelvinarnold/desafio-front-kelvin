import React from 'react';
import { Link } from 'react-router-dom'

const Navigation = () => {
  return (
    <div className="app-component--navigation">
      <div className="container">
        <Link className="items title text-white" to="/">All Characteres</Link>
      </div>
    </div>
  )
}

export default Navigation;