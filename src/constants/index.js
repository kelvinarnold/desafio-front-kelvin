// Images
export const IMG_LOGO = require('./../assets/images/marvel-logo.png');
export const IMG_SITE_BLINDADO = require('./../assets/images/site-blindado.png');

//Navigation
export const FILTER_NIVEL_1 = [
  {id: 10001, name: `Comics`},
  {id: 10002, name: `Events`},
  {id: 10003, name: `Series`}
]