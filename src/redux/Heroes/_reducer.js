import * as types from './_action.types';

const initialState = {
  heroe: {},
  heroesList: [],
  filter: [],
  loadingHeroe: true,
  loadingHeroes: true,
  loadingFilter: false,
  error: false
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
  // GET HEROES
  case `${types.GET_ALL_HEROES}_PENDING`:
    return { ...state, loadingHeroes: true, error: false }
  case `${types.GET_ALL_HEROES}_FULFILLED`:
    return { ...state, heroesList: [...payload.results], loadingHeroes: false }
  case `${types.GET_ALL_HEROES}_REJECTED`:
    return { ...state, loadingHeroes: false, error: true }
  // GET HEROES BY ID
  case `${types.GET_HEROES_BY_ID}_PENDING`:
    return { ...state, loadingHeroe: true, error: false }
  case `${types.GET_HEROES_BY_ID}_FULFILLED`:
    return { ...state, heroe: payload.results[0] || {}, loadingHeroe: false }
  case `${types.GET_HEROES_BY_ID}_REJECTED`:
    return { ...state, loadingHeroe: false, error: true }
  // GET COMICS
  case `${types.GET_COMICS}_PENDING`:
    return { ...state, loadingFilter: true, error: false }
  case `${types.GET_COMICS}_FULFILLED`:
    return { ...state, filter: [...payload.results], loadingFilter: false }
  case `${types.GET_COMICS}_REJECTED`:
    return { ...state, loadingFilter: false, error: true }
  // GET HEROES BY COMIC
  case `${types.GET_HEROES_BY_COMIC}_PENDING`:
    return { ...state, loadingHeroes: true, error: false }
  case `${types.GET_HEROES_BY_COMIC}_FULFILLED`:
    return { ...state, heroesList: [...payload.results], loadingHeroes: false }
  case `${types.GET_HEROES_BY_COMIC}_REJECTED`:
    return { ...state, loadingHeroes: false, error: true }
  // GET EVENTS
  case `${types.GET_EVENTS}_PENDING`:
    return { ...state, loadingFilter: true, error: false }
  case `${types.GET_EVENTS}_FULFILLED`:
    return { ...state, filter: [...payload.results], loadingFilter: false }
  case `${types.GET_EVENTS}_REJECTED`:
    return { ...state, loadingFilter: false, error: true }
  // GET HEROES BY EVENT
  case `${types.GET_HEROES_BY_EVENT}_PENDING`:
    return { ...state, loadingHeroes: true, error: false }
  case `${types.GET_HEROES_BY_EVENT}_FULFILLED`:
    return { ...state, heroesList: [...payload.results], loadingHeroes: false }
  case `${types.GET_HEROES_BY_EVENT}_REJECTED`:
    return { ...state, loadingHeroes: false, error: true }
  // GET SERIES
  case `${types.GET_SERIES}_PENDING`:
    return { ...state, loadingFilter: true, error: false }
  case `${types.GET_SERIES}_FULFILLED`:
    return { ...state, filter: [...payload.results], loadingFilter: false }
  case `${types.GET_SERIES}_REJECTED`:
    return { ...state, loadingFilter: false, error: true }
  // GET HEROES BY SERIES
  case `${types.GET_HEROES_BY_SERIE}_PENDING`:
    return { ...state, loadingHeroes: true, error: false }
  case `${types.GET_HEROES_BY_SERIE}_FULFILLED`:
    return { ...state, heroesList: [...payload.results], loadingHeroes: false }
  case `${types.GET_HEROES_BY_SERIE}_REJECTED`:
    return { ...state, loadingHeroes: false, error: true }
  default:
    return state
  }
}
