import * as types from './_action.types';
import { apiHeroes } from './../../service/api';

const getHeroes = () => {
  return {
    type: types.GET_ALL_HEROES,
    payload: apiHeroes.getHeroes()
  }
}

const getHeroeByID = id => {
  return {
    type: types.GET_HEROES_BY_ID,
    payload: apiHeroes.getHeroesByID(id)
  }
}

const getComics = () => {
  return {
    type: types.GET_COMICS,
    payload: apiHeroes.getComics()
  }
}

const getHeroeByComic = id => {
  return {
    type: types.GET_HEROES_BY_COMIC,
    payload: apiHeroes.getHeroesByComic(id)
  }
}

const getSeries = () => {
  return {
    type: types.GET_SERIES,
    payload: apiHeroes.getSeries()
  }
}

const getHeroesBySerie = id => {
  return {
    type: types.GET_HEROES_BY_COMIC,
    payload: apiHeroes.getHeroesBySerie(id)
  }
}

const getEvents = () => {
  return {
    type: types.GET_EVENTS,
    payload: apiHeroes.getEvents()
  }
}

const getHeroesByEvent = id => {
  return {
    type: types.GET_HEROES_BY_COMIC,
    payload: apiHeroes.getHeroesByEvent(id)
  }
}

export default {
  getHeroes,
  getHeroeByID,
  getComics,
  getHeroeByComic,
  getSeries,
  getHeroesBySerie,
  getEvents,
  getHeroesByEvent
}