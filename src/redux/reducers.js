import { combineReducers } from 'redux';
// Reducers
import { reducer as HeroesReducer } from './Heroes'

const reducers = combineReducers({
  heroes: HeroesReducer
})

export default reducers;